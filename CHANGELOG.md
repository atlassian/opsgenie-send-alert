# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.4.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.4.0

- minor: Bump setuptools to fix vulnerabilities.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 1.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.2.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.1.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerabilities with certify and gitpython.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 1.0.1

- patch: Improve changelog. Breaking change: DESCRIPTION, SOURCE and PRIORITY are not used anymore. Now you should use PAYLOAD instead.

## 1.0.0

- major: Migrate pipe's codebase to Python. Now the pipe is based on opsgenie python sdk.
- patch: Update community link.
- patch: Update release process.

## 0.5.3

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.5.2

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.5.1

- patch: Internal maintenance: Add gitignore secrets.

## 0.5.0

- minor: Add support for EU endpoint.

## 0.4.2

- patch: Update the Readme with a new Atlassian Community link.

## 0.4.1

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.4.0

- minor: A link to the new alert is now displayed in the pipe logs

## 0.3.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.3

- patch: Updated contributing guidelines

## 0.2.2

- patch: Fixed the bug with handling doublequotes in a payload

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Switch from task to pipe naming conventions.

## 0.1.2

- patch: Improve logging

## 0.1.1

- patch: Use double quotes for the YAML definition

## 0.1.0

- minor: Initial version
