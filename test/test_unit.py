from http import HTTPStatus
import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from opsgenie_sdk import ApiException
from unittest import TestCase

import pytest

from pipe.pipe import OpsgenieSendAlertPipe, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CreateAlertCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker):
        self.caplog = caplog
        self.mocker = mocker

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'GENIE_KEY': 'test-api-key',
                'MESSAGE': 'test-message'
            }
        )

        pipe = OpsgenieSendAlertPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = self.mocker.Mock()

        with capture_output() as out:
            pipe.run()

        self.assertIn('✔ Pipe has finished successfully.', out.getvalue())

    def test_create_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'GENIE_KEY': 'test-api-key',
                'MESSAGE': 'test-message'
            }
        )

        api_mock = self.mocker.Mock()

        api_mock.create_alert.side_effect = ApiException(reason='mocked error', status=HTTPStatus.NOT_FOUND)

        pipe = OpsgenieSendAlertPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = api_mock

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertIn('Pipe has finished with an error: (404)\nReason: mocked error', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_get_max_attempts_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'GENIE_KEY': 'test-api-key',
                'MESSAGE': 'test-message'
            }
        )

        self.mocker.patch('pipe.pipe.MAX_WAIT_ATTEMPTS', 1)

        api_mock = self.mocker.Mock()

        api_mock.get_request_status.side_effect = ApiException(reason='mocked error', status=HTTPStatus.NOT_FOUND)

        pipe = OpsgenieSendAlertPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = api_mock

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertIn('Pipe has finished with an error: (404)\nReason: mocked error', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_get_api_error_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'GENIE_KEY': 'test-api-key',
                'MESSAGE': 'test-message'
            }
        )

        self.mocker.patch('pipe.pipe.MAX_WAIT_ATTEMPTS', 2)

        api_mock = self.mocker.Mock()

        api_mock.get_request_status.side_effect = ApiException(reason='mocked error', status=HTTPStatus.BAD_REQUEST)

        pipe = OpsgenieSendAlertPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = api_mock

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertIn('Pipe has finished with an error: (400)\nReason: mocked error', out.getvalue())
        api_mock.get_request_status.assert_called_once()
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_no_message_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'GENIE_KEY': 'test-api-key'
            }
        )
        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                OpsgenieSendAlertPipe(
                    schema=schema, check_for_newer_version=True)

        self.assertIn('MESSAGE:\n- required field', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)
