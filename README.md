# Bitbucket Pipelines Pipe: Opsgenie send alert

Sends an alert to [Opsgenie][Opsgenie].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/opsgenie-send-alert:1.4.1
  variables:
    GENIE_KEY: '<string>'
    MESSAGE: '<string>'
    # REGION: '<string>' # Optional.
    # PAYLOAD: '<json>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable      | Usage                                                                                                                                                                                                                          |
|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GENIE_KEY (*) | API key. It is recommended to use a secure repository variable.                                                                                                                                                                |
| MESSAGE (*)   | Alert message.                                                                                                                                                                                                                 |
| REGION        | Opsgenie [Service region][service region]. If `eu` then use `"https://api.eu.opsgenie.com/v2/alerts"` endpoint, else - `"https://api.opsgenie.com/v2/alerts"`. Valid options are `eu`, `us`. Default: `us`.                    |
| PAYLOAD       | JSON document containing payload supported variables for [creating alert][get alert]. The value should be a dictionary(mapping) {"key": "value"}. The Examples section below contains an example for passing payload to alert. |
| DEBUG         | Turn on extra debug information. Default: `false`.                                                                                                                                                                             |

_(*) = required variables._

More info about parameters and values can be found in the Opsgenie official documentation: https://docs.opsgenie.com/docs/alert-api#section-create-alert.

## Prerequisites

To send alerts to Opsgenie, you need an API key. You can follow the instructions [here](https://docs.opsgenie.com/docs/api-integration) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/opsgenie-send-alert:1.4.1
    variables:
      GENIE_KEY: $GENIE_KEY
      MESSAGE: 'Hello, world!'
```

Send alert to the EU instance of Opsgenie

```yaml
script:
  - pipe: atlassian/opsgenie-send-alert:1.4.1
    variables:
      GENIE_KEY: $GENIE_KEY
      MESSAGE: 'Hello, world!'
      REGION: 'eu'
```

Advanced example with payload usage:

```yaml
script:
  - pipe: atlassian/opsgenie-send-alert:1.4.1
    variables:
      GENIE_KEY: $GENIE_KEY
      MESSAGE: 'Hello, world!'
      PAYLOAD:  >
        {
           "description": "Alert from <a href='https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}'>Pipeline #${BITBUCKET_BUILD_NUMBER}</a>",
           "source": "Bitbucket Pipelines",
           "priority": "P1",
           "visible_to":[
             {
                "name":"rocket_team",
                "type":"team"
              },
              {
                "username":"user@mail.com",
                "type":"user"
              }]
        }
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,opsgenie
[get alert]: https://github.com/opsgenie/opsgenie-python-sdk/blob/master/docs/CreateAlertPayload.md
[Opsgenie]: https://www.opsgenie.com
[service region]: https://docs.opsgenie.com/docs/european-service-region
