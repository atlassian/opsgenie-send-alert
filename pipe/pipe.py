from http import HTTPStatus
from time import sleep

import opsgenie_sdk
from opsgenie_sdk.rest import ApiException
import yaml

from bitbucket_pipes_toolkit import Pipe

MAX_WAIT_ATTEMPTS = 60

schema = {
    "GENIE_KEY": {"required": True, "type": "string"},
    "MESSAGE": {"required": True, "type": "string"},
    "REGION": {"required": False, "type": "string", "default": "us", "allowed": ["eu", "us"]},
    "PAYLOAD": {"required": False, "type": "dict", "empty": False},
    "DEBUG": {"required": False, "type": "boolean", "default": False}
}


class OpsgenieSendAlertPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.payload = self.get_variable("PAYLOAD")
        self.message = self.get_variable("MESSAGE")
        # create an instance of the API class
        self.api_instance = self.from_api_key()

    def from_api_key(self):
        configuration = opsgenie_sdk.Configuration()
        # Configure API key authorization: GenieKey
        configuration.api_key['Authorization'] = self.get_variable("GENIE_KEY")

        if self.get_variable("REGION") == "eu":
            configuration.host = 'https://api.eu.opsgenie.com'

        return opsgenie_sdk.AlertApi(opsgenie_sdk.ApiClient(configuration))

    def wait_for_alert(self, request_id):
        count = 1
        alert_id = None

        while count <= MAX_WAIT_ATTEMPTS and alert_id is None:
            try:
                resp = self.api_instance.get_request_status(request_id)
                alert_id = resp.data.alert_id
            except ApiException as e:
                if count == MAX_WAIT_ATTEMPTS and alert_id is None or e.status != HTTPStatus.NOT_FOUND:
                    self.fail(f'Pipe has finished with an error: {e}')

            count += 1
            sleep(1)

        return alert_id

    def run(self):
        self.log_info('Creating opsgenie alert...')

        payload = self.payload or dict()
        payload.update({"message": self.message})
        create_alert_payload = opsgenie_sdk.CreateAlertPayload(**payload)

        try:
            # Create Alert
            api_response = self.api_instance.create_alert(create_alert_payload)
        except ApiException as e:
            self.fail(f'Pipe has finished with an error: {e}')

        alert_id = self.wait_for_alert(api_response.request_id)

        self.log_info(f"Id of the opsgenie created alert: {alert_id}")

        self.success('Pipe has finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = OpsgenieSendAlertPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
